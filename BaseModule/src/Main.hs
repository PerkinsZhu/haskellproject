module Main where

main :: IO ()
main = putStrLn "Hello, Haskell!"


{-
构建方式：
    直接手动创建项目目录和module目录，然后执行一下命令
    cabal init
    cabal configure
    cabal build
    echo "" > .cabal // cmd 创建隐藏文件
    用idea打开目录
-}