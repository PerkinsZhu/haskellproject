import Data.List
import Data.List hiding (nub)
import Data.List (nub,sort)
import qualified Data.Map as M
import System.IO
size :: Int
size  = 5
sumOfSize = 2 * size
--removeNonUppercase :: [Char] -> [Char]
--removeNonUppercase st = [ c | c  st, c `elem` ['A'..'Z']]
addThree :: Int -> Int -> Int -> Int
addThree x y z = x + y + z
factorial :: Integer -> Integer
factorial n = product [1..n]
circumference :: Float -> Float
circumference r = 2 * pi * r
circumference' :: Double -> Double
circumference' r = 2 * pi * r

sayMe :: (Integral a) => a -> String
sayMe 1 = "One!"
sayMe 2 = "Two!"
sayMe 3 = "Three!"
sayMe 4 = "Four!"
sayMe 5 = "Five!"
sayMe x = "Not between 1 and 5"
{-
factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n - 1)
-}


length' :: (Num b) => [a] -> b
length' [] = 0
length' (_:xs) = 1 + length' xs

capital :: String -> String
capital "" = "Empty string, whoops!"
capital all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]

numUniques :: (Eq a) => [a] -> Int
numUniques = length . nub


{-
 ghc --make body -- 编译生成body.exe body.sh
 运行启动脚本即可
-}

--main = putStrLn "Hello, world!"
x = 2                   -- 两个连字符号表示注解
y = 3                   --

{-
main = let z = x + y    -- let 引入一个本地绑定
       in print z       -- 程序将打印 5
-}

main = print (numUniques "asdfkw3elewlk")